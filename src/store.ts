import Vue from "vue";
import Vuex from "vuex";
import mutationsFile from "@/mutations.ts";
import actionFile from "@/actions.ts";
import gettersFile from "@/getters.ts";

Vue.use(Vuex);

interface Product {
  id: number;
  title: String;
  price: number;
  inventory: number;
}

const products: Array<Product> = [
  { id: 1, title: "iPad 4 Mini", price: 500.01, inventory: 2 },
  { id: 2, title: "H&M T-Shirt White", price: 10.99, inventory: 10 },
  { id: 3, title: "Charli XCX - Sucker CD", price: 19.99, inventory: 5 }
];

const productsCart: Array<Product> = [];

export default new Vuex.Store({
  state: {
    products: products,
    productsCart: productsCart
  },
  mutations: mutationsFile,
  actions: actionFile,
  getters: gettersFile
});
