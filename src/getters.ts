interface Product {
  id: number;
  title: String;
  price: number;
  inventory: number;
}

export default {
  cartTotal(state: any, getters: any) {
    let total: number = 0;
    for (let i = 0; i < state.productsCart.length; i++) {
      total += state.productsCart[i].price * state.productsCart[i].inventory;
    }
    return total;
  }
};
