interface Product {
    id: number;
    title: String;
    price: number;
    inventory: number;
  }

export default{
  pushProductToCart(state:any, product:Product) {
    state.productsCart.push({
      id: product.id,
      title: product.title,
      price: product.price,
      inventory: 1
    });
  },
  incrementItemQuantity(state:any, cartItem:Product) {
    cartItem.inventory++;
  },
  decrementProductInventory(state:any, product:Product) {
    product.inventory--;
  }
}