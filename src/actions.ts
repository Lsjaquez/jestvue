interface Product {
  id: number;
  title: String;
  price: number;
  inventory: number;
}

export default {
  addProductToCart(context: any, product: Product) {
    if (product.inventory > 0) {
      const cartItem = context.state.productsCart.find(function(
        element: Product
      ) {
        return element.id === product.id;
      });
      if (!cartItem) {
        context.commit("pushProductToCart", product);
      } else {
        context.commit("incrementItemQuantity", cartItem);
      }
    }
    context.commit("decrementProductInventory", product);
  }
};
