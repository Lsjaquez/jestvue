import { shallowMount } from "@vue/test-utils";
import Vue from "vue";
import Vuex from "vuex";
import Cart from "@/components/ShoppingCart.vue";
import mutations from "@/mutations";
import action from "@/actions";

Vue.use(Vuex);

describe("Test Mutatios", () => {
  interface Product {
    id: number;
    title: String;
    price: number;
    inventory: number;
  }

  test("Push Product to empty cart", () => {
    const productsCart: Array<Product> = [];
    const state = {
      productsCart: productsCart
    };
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 1
    };
    mutations.pushProductToCart(state, product);
    expect(state.productsCart[0]).toEqual(product);
  });

  test("incrementItemQuantityInCart", () => {
    const state = {};
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 1
    };
    mutations.incrementItemQuantity(state, product);
    expect(product.inventory).toEqual(2);
  });

  test("decrementProductInventory", () => {
    const state = {};
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 1
    };
    mutations.decrementProductInventory(state, product);
    expect(product.inventory).toEqual(0);
  });
});

describe("Test Action", () => {
  interface Product {
    id: number;
    title: String;
    price: number;
    inventory: number;
  }

  const products: Array<Product> = [
    { id: 1, title: "iPad 4 Mini", price: 500.01, inventory: 2 },
    { id: 2, title: "H&M T-Shirt White", price: 10.99, inventory: 10 },
    { id: 3, title: "Charli XCX - Sucker CD", price: 19.99, inventory: 5 }
  ];

  const mutations = {
    pushProductToCart(state: any, product: Product) {
      state.productsCart.push({
        id: product.id,
        title: product.title,
        price: product.price,
        inventory: 1
      });
    },
    incrementItemQuantity(state: any, cartItem: Product) {
      cartItem.inventory++;
    },
    decrementProductInventory(state: any, product: Product) {
      product.inventory--;
    }
  };

  test("add 1 product to cart and length = 1", () => {
    const state = {
      products: products,
      productsCart: []
    };

    const store = new Vuex.Store({
      state,
      mutations
    });
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 5
    };
    action.addProductToCart(store, product);
    expect(state.productsCart.length).toEqual(1);
  });

  test("add same product twice to cart and length = 1", () => {
    const state = {
      products: products,
      productsCart: []
    };

    const store = new Vuex.Store({
      state,
      mutations
    });
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 5
    };
    action.addProductToCart(store, product);
    action.addProductToCart(store, product);
    expect(state.productsCart.length).toEqual(1);
  });

  test("add 2 different products to cart and length = 2", () => {
    const state = {
      products: products,
      productsCart: []
    };

    const store = new Vuex.Store({
      state,
      mutations
    });
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 5
    };
    const product2: Product = {
      id: 4,
      title: "Mac",
      price: 19.99,
      inventory: 8
    };
    action.addProductToCart(store, product);
    action.addProductToCart(store, product2);
    expect(state.productsCart.length).toEqual(2);
  });

  test("block add if inventory of product = 0", () => {
    const productsCart: Array<Product> = [];
    const state = {
      products: products,
      productsCart: productsCart
    };

    const store = new Vuex.Store({
      state,
      mutations
    });
    const product: Product = {
      id: 3,
      title: "Charli XCX - Sucker CD",
      price: 19.99,
      inventory: 2
    };
    action.addProductToCart(store, product);
    action.addProductToCart(store, product);
    action.addProductToCart(store, product);
    expect(state.productsCart.length).toEqual(1);
    expect(state.productsCart[0].inventory).toEqual(2);
  });
});
